#!/bin/sh
DOCKERFILE=./kubernetes-ingress/build/Dockerfile
sed -i 's/amd64/arm64/g' $DOCKERFILE
sed -i 's/DUMB_INIT_SHA256=.*$/DUMB_INIT_SHA256=45b1bbf56cc03edda81e4220535a025bfe3ed6e93562222b9be4471005b3eeb3/' $DOCKERFILE
sed -i 's/haproxytech\/haproxy-alpine:2.1/haproxy:lts-alpine/' $DOCKERFILE
